import React , {Fragment} from 'react';
import { Route,Switch } from 'react-router-dom';
import Login from "./Components/auth/Login";
// import Layout from "./Components/Layout/Layout";
import Routes from './Components/router/Routes';


function App() {
  return (
    <Fragment>
      <Switch>
      <Route exact path="/" component={Login} />      
      <Route component={Routes}/>
      </Switch>
    </Fragment>
  );
}

export default App;
