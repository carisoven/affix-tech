const express = require("express");
const Comments = require("../../models/Comments");
const Content = require("../../models/Content");
const router = express.Router();


//  Add Content 
//  api/content/addcontent
router.post("/addcontent",async (req,res)=>{
    try {
        
        const newcontent = new Content({
            contenttitle:req.body.contenttitle,
            contentdata:req.body.contentdata,
            viewcontent:0,
            contenttype:req.body.contenttype
        });

        const contentadd = await newcontent.save();
        res.json(contentadd);
        console.log(contentadd);

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});

// View Only Content
// /api/content/view/:id
router.get("/view/:id",async (req,res)=>{
    try {
        const content = await Content.findById(req.params.id);
        console.log(content);
        res.json(content);
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
})

// View Count Content (Only Click)
// /api/content/:id
router.get("/:id",async (req,res) =>{
    try {
        const content = await Content.findById(req.params.id);
        const viewcounts = {
            viewcontent: content.viewcontent+1
        }
        const countview = await Content.findByIdAndUpdate(req.params.id,{$set:viewcounts},{new:true})
        console.log(countview);
        res.json(countview);
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});





//  Update Content
//  api/content/edit/:id
router.post("/edit/:id",async (req,res) =>{
    try {
        const oldcontent = await Content.findById(req.params.id);
        const newcontent = {
            contenttitle:   req.body.contenttitle,
            contentdata:    req.body.contentdata,
            contenttype:    req.body.contenttype
        }
        const updatecontnet = await Content.findByIdAndUpdate(req.params.id,{$set:newcontent},{new:true})
        res.json(updatecontnet)
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});


router.delete("/del/:id",async (req,res) =>{
    try {
        const content = await Content.findById(req.params.id);
        console.log(content);
        await Content.findByIdAndRemove(req.params.id);
        res.send("Delete Content Successfuly");
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
})

module.exports = router;
