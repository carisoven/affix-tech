const express = require("express");
const router = express.Router();

const User = require("../../models/User");
const passport = require("passport");
const auth = require("../../middleware/auth");

router.get("/", async (req, res) => {
  // try {
  //   const user = await User.findById(req.user.id).select("-hash -salt");
  //   res.json(user);
  //   console.log(user);
  // } catch (err) {
  //   console.error(err.message);
  //   res.status(500).send("Server Error");
  // }
  if (req.user) {
    res.status(200).json({
      userid: req.user.id,
      success: true,
      status: "Load User Successful!",
    });
  } else {
    res.status(500).json({
      success: false,
      status: "No Authenticator user",
    });
  }
});

router.post("/signup", (req, res) => {
  User.register(
    new User({
      username: req.body.username,
    }),
    req.body.password,
    (err, user) => {
      if (err) {
        res.statusCode = 500;
        res.setHeader("Content-Type", "application/json");
        res.json({
          err: err,
        });
      } else {
        passport.authenticate("local",)(req, res, () => {
          User.findOne(
            {
              username: req.body.username,
            },
            (err, person) => {
              res.statusCode = 200;
              res.setHeader("Content-Type", "application/json");
              res.json({
                userid: req.user.id,
                success: true,
                status: "Registration Successful!",
              });
            }
          );
        });
      }
    }
  );
});

router.post(
  "/login",
  passport.authenticate("local", { failureRedirect: "/failed" }),
  (req, res) => {
    User.findOne(
      {
        username: req.body.username,
      },
      (err, person) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json({
          userid: req.user.id,
          success: true,
          status: "You are successfully logged in!",
        });
      }
    );
  }
);

// Auth Routes
// Go to Login with Google
router.get(
  "/google",
  passport.authenticate("google", { scope: [ 'email', 'profile' ]  })
);
// Back to Login with Google
router.get(
  "/google/callback",
  passport.authenticate("google", { failureRedirect: "/failed" }),
  (req, res) => {
    // Successful authentication, redirect home page.
    res.json({
   
      userid: req.user.id,
      success: true,
      status: "You are successfully logged in!",
    });
  }
);

router.get("/failed", (req, res) => {
  res.send("You entered the Fail");
});

router.get("/logout", (req, res) => {
  req.logout();
  res.json({
    success: false,
    status: "You are successfully logged Out!",
  });
});

module.exports = router;
