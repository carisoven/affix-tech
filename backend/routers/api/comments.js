const express = require("express");
const router = express.Router();


const Comments = require("../../models/Comments");
const Content = require("../../models/Content");

router.post("/addcomment",async (req,res)=>{

    try {
        
        const contents = await Content.findById(req.body.contentid)

        const newcomment = new Comments({
            commentdetail:req.body.commentdetail,
            contentid:contents.id 
        });

        const commentcontent = await newcomment.save();
        
        res.json(commentcontent);
        console.log(commentcontent);
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
})

module.exports = router;
