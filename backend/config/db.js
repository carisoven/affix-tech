const mongoose = require('mongoose');
// const config = require('config');
// const db = config.get('mongoURL');
require('dotenv').config();
const connectDB = async ()=>{
    try{
        await mongoose.connect(process.env.MONGO_URI,{
            useNewUrlParser:true,
        });
        console.log("MongoDB Connected..");
    }catch(err){
        console.log("MongoDB not Connected...")
        console.error(err.massege);
        process.exit(1);
    }
};
module.exports = connectDB;