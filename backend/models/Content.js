const mongoose = require("mongoose");

const ContentSchema = new mongoose.Schema({
    contenttitle:{
        type:String,
        required:true
    },
    contentdata:{
        type:String,
        required:true
    },
    contentcomment:{
        type: mongoose.Schema.Types.ObjectId,
        ref:"comments"
    },
    viewcontent:{
        type:Number
    },
    contenttype:{
        type:String,
        required:true
    },
    userid:{ 
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    }

});


module.exports = Content = mongoose.model("content", ContentSchema);
