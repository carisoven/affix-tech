const mongoose = require("mongoose");

const CommentsSchema = new mongoose.Schema({
    commentdetail:{
        type:String,
        require:true
    },
    contentid:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"content"
    }

});


module.exports = Comments = mongoose.model("Comments", CommentsSchema);
