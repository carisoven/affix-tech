const mongoose = require("mongoose");
const passportLocalMongoose = require("passport-local-mongoose");

const UserSchema = new mongoose.Schema({
  fullname: { type: String },
  username: { type: String },
  hash: { type: String },
  salt: { type: String },
  role: {
    type: String,
    default: "user",
    enum: ["user", "admin"],
    required: true,
  },
  picture: { type: String },
  providerId: { type: String },
  providerData: {},
});

UserSchema.plugin(passportLocalMongoose);

module.exports = User = mongoose.model("user", UserSchema);
