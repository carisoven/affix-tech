const express = require("express");
const connectDB = require("./config/db");
const bodyParser = require('body-parser');

const app = express();
const cors = require('cors')
require('dotenv').config()
//authentication
const passport = require('passport');
require('./middleware/passport-setup');
const auth =require("./middleware/auth");
const cookieSession = require('cookie-session');
const cookieParser = require('cookie-parser')
//Connect MongoDB
connectDB();

// Init Middleware	
app.use(express.json({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());
// app.use(morgan('dev'));

// set up session cookies
app.use(
  cookieSession({
    maxAge: 7 * 24 * 60 * 60 * 1000,
    keys: [process.env.COOKIEKEY],
    saveUninitialized: false,
    resave: false
  })
);
app.use(cookieParser());

// Initializes passport and passport sessions
app.use(passport.initialize());
app.use(passport.session());

//Router\api
app.get("/", (req, res) => res.send("API Running"));
app.use('/api/auth', require("./routers/api/auth"));
app.use('/api/comments',require("./routers/api/comments"));
app.use('/api/content',require("./routers/api/content"));
// app.use("/api/user", require("./router/api/user"));
// app.use("/api/auth", require("./router/api/auth"));
// app.use("/api/marketing", require("./router/api/marketing"));
app.get("/api/index",auth, (req, res) => res.send("Login Complete"))


// // Serve static assets in production
// if (process.env.NODE_ENV === 'production') {
//     // Set static folder	
//     app.use(express.static('client/build'));
//     app.get('*', (req, res) => {
//         res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
//     });
// }

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`server Started on port ${PORT}`));